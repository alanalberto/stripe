<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CostumersComponent extends Component
{
    public $clientShow = false;
    public $costumer;
    
    public function render()
    {

        $stripe = new \Stripe\StripeClient( 'sk_test_51I0xwdBSAzMVH2c76HxZ187r5C0nbI2znPxvJ8rtMvuKFMEqpv6fXtaShSkFgkFyYIZKjmVKqVyyQFtKfEaHQjJI00wqkYoApd' );        
        return view('livewire.costumers-component',[
            'Costumers' => $stripe->customers->all(),
        ])->layout('layouts.app');
    }

    public function showCostumer($id_costumer)
    {

        $this->clientShow = true;

        $stripe = new \Stripe\StripeClient('sk_test_51I0xwdBSAzMVH2c76HxZ187r5C0nbI2znPxvJ8rtMvuKFMEqpv6fXtaShSkFgkFyYIZKjmVKqVyyQFtKfEaHQjJI00wqkYoApd');
        $this->costumer = $stripe->customers->retrieve( 'cus_IcFMqrqm6YUQeN',[]);
    }

}
