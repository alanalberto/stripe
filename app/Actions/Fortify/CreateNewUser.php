<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
        ])->validate();

        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);


        $stripe = new \Stripe\StripeClient(
            'sk_test_51I0xwdBSAzMVH2c76HxZ187r5C0nbI2znPxvJ8rtMvuKFMEqpv6fXtaShSkFgkFyYIZKjmVKqVyyQFtKfEaHQjJI00wqkYoApd'
          );

        $stripe->customers->create([
            'email' => $user->email,
            'name' => $user->name,
            'description' => 'vendedor comun',
        ]);

        return $user;
          
    }
}
