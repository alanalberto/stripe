<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        Listado de clientes
    </h2>
</x-slot>

<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">



            <table class="min-w-full divide-y divide-gray-200">
                <thead class="bg-gray-50">
                    <tr>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">

                            Id del cliente
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Name
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Status
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            descipcion
                        </th>
                        <th scope="col" class="relative px-6 py-3">
                            <span class="sr-only">Edit</span>
                        </th>
                    </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                    @foreach($Costumers->data as $item)
                    <tr>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm text-gray-900">
                                {{$item->id}}
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="flex items-center">

                                <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900">
                                        {{$item->name}}
                                    </div>
                                    <div class="text-sm text-gray-500">
                                        {{$item->email}}
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <span
                                class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                Active
                            </span>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                            {{$item->description}}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">


                            <button
                                class="py-1 px-2 bg-blue-800 text-white rounded-full mx-2 shadow-xl hover:bg-blue-900 focus:outline-none"
                                wire:click="showCostumer(1)">
                                Show user
                            </button>
                            <button
                                class="py-1 px-2 bg-red-700 text-white rounded-full mx-2 shadow-xl hover:bg-red-600 focus:outline-none"
                                wire:click="delCostumer">
                                Borrar
                            </button>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>


    @if($clientShow == true)
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 pt-10">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

            {{$costumer}}
        </div>
    </div>
    @endif


</div>
